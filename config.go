// Copyright (c) 2019 Anbillon Team (anbillonteam@gmail.com).
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package consul

import (
	"fmt"

	"github.com/spf13/viper"
	_ "github.com/spf13/viper/remote"
	"gitlab.com/anbillon/fiber"
	"gitlab.com/anbillon/slago"
)

// ConfigProperty defines the property of fiber.consul.config section in yaml config.
// This will be used to configure consul config server.
type ConfigProperty struct {
	Enabled    bool   `fiber:"enabled"`
	ConfPrefix string `fiber:"prefix"`
	Format     string `fiber:"format"`
	Name       string `fiber:"name"`
}

func (p *ConfigProperty) Prefix() string {
	return "fiber.consul.config"
}

type confLoader struct {
	consulProperty *BaseProperty
	configProperty *ConfigProperty
}

func init() {
	fiber.Wire(&ConfigProperty{
		Enabled:    true,
		ConfPrefix: "config",
		Format:     "yaml",
	})
	fiber.Wire(newConsulConfigLoader)
}

func newConsulConfigLoader(consulProperty *BaseProperty,
	configProperty *ConfigProperty) *confLoader {
	return &confLoader{
		consulProperty: consulProperty,
		configProperty: configProperty,
	}
}

func (c *confLoader) Initialize(ctx *fiber.AppContext) {
	slago.Logger().Info().Msg("initializing consul config loader")

	if !c.configProperty.Enabled {
		slago.Logger().Info().Msg("consul config is disabled, skipping")
		return
	}

	if len(c.configProperty.Name) == 0 {
		c.configProperty.Name = ctx.GetString("fiber.application.name")
	}

	var supported = false
	for _, ext := range viper.SupportedExts {
		if ext == c.configProperty.Format {
			supported = true
			break
		}
	}

	if !supported {
		slago.Logger().Fatal().Msgf("unsupported config format for consul config sever: %s",
			c.configProperty.Format)
	}

	// create a new viper to read consul remote configuration
	consulViper := viper.New()
	consulViper.SetConfigType(c.configProperty.Format)

	// splice config path
	var configName string
	if len(ctx.Profile()) == 0 {
		configName = c.configProperty.Name
	} else {
		configName = fmt.Sprintf("%s-%s", c.configProperty.Name, ctx.Profile())
	}
	configPath := fmt.Sprintf("%v/%v", c.configProperty.ConfPrefix, configName)
	if err := consulViper.AddRemoteProvider("consul",
		c.consulProperty.Address, configPath); err != nil {
		// this will not occur
		return
	}

	slago.Logger().Info().Msgf("fetch config from consul: %s", c.consulProperty.Address)

	if err := consulViper.ReadRemoteConfig(); err != nil {
		_, ok := err.(viper.RemoteConfigError)
		var message string
		if ok {
			message = fmt.Sprintf("fail to load remote config in %v", configPath)
		}
		slago.Logger().Error().Err(err).Msg(message)
		return
	}

	remoteConfigMap := consulViper.AllSettings()
	// remove all property for consul config
	delete(remoteConfigMap, c.configProperty.Prefix())
	// merge remote config into local config
	if err := ctx.MergeConfigMap(remoteConfigMap); err != nil {
		return
	}

	// re-unmarshal properties after load remote configuration
	for _, property := range fiber.ConfigProperties() {
		err := ctx.UnmarshalStruct(property.Prefix(), property)
		if err != nil {
			slago.Logger().Fatal().Err(err).Msgf(
				"fail to unmarshal property: %s", property.Prefix())
		}
	}

	// reinitialize logging after consul config source loaded
	if err := fiber.LoggingSystem().Reinitialize(ctx); err != nil {
		slago.Logger().Fatal().Err(err).Msg("fail to re-initialize logging system")
	}
}
