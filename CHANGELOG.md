# 0.0.3
* Update fiber version to v0.0.5 and consul properties.
* Use slago to output log.

# 0.0.2
* Re-unmarshal properties in config.

# 0.0.1
* First implementation of consul config and discovery client for fiber.
