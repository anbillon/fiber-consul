module gitlab.com/anbillon/fiber-consul

go 1.12

require (
	github.com/hashicorp/consul/api v1.3.0
	github.com/spf13/viper v1.5.0
	gitlab.com/anbillon/fiber v0.0.5
	gitlab.com/anbillon/slago v0.3.0
)
