// Copyright (c) 2019 Anbillon Team (anbillonteam@gmail.com).
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package consul

import (
	"fmt"
	"net"
	"os"

	"github.com/hashicorp/consul/api"
	"gitlab.com/anbillon/fiber"
	"gitlab.com/anbillon/slago"
)

// DiscoveryProperty defines the property of fiber.consul.discovery
// section in yaml config.
type DiscoveryProperty struct {
	Enabled             bool     `fiber:"enabled"`
	ServiceName         string   `fiber:"service-name"`
	ServiceId           string   `fiber:"service-id"`
	PreferIpAddress     bool     `fiber:"prefer-ip-address"`
	IpAddress           string   `fiber:"ip-address"`
	Port                int      `fiber:"port"`
	HealthCheckUrl      string   `fiber:"health-check-url"`
	HealthCheckInterval string   `fiber:"health-check-interval"`
	Tags                []string `fiber:"tags"`
}

type discoveryClient struct {
	consulClient      *api.Client
	discoveryProperty *DiscoveryProperty
	consulProperty    *BaseProperty
}

func (*DiscoveryProperty) Prefix() string {
	return "fiber.consul.discovery"
}

func init() {
	fiber.Wire(&DiscoveryProperty{
		Enabled:             true,
		HealthCheckInterval: "1m",
	}, newDiscoveryClient)
}

func newDiscoveryClient(consulProperty *BaseProperty,
	discoveryProperty *DiscoveryProperty,
	appProperty *fiber.ApplicationProperty,
	serverProperty *fiber.ServerProperty) *discoveryClient {
	hostname, err := os.Hostname()
	if err != nil {
		slago.Logger().Fatal().Err(err).Msg("fail to initialize consul discovery")
	}

	if len(discoveryProperty.IpAddress) == 0 {
		ip := getIp()
		if len(ip) != 0 {
			discoveryProperty.IpAddress = ip
		} else {
			discoveryProperty.IpAddress = hostname
		}
	}

	if len(discoveryProperty.ServiceId) == 0 {
		discoveryProperty.ServiceId = appProperty.Name
	}
	if len(discoveryProperty.ServiceName) == 0 {
		discoveryProperty.ServiceName = appProperty.Name
	}
	if discoveryProperty.Port == 0 {
		discoveryProperty.Port = serverProperty.Port
	}
	if len(discoveryProperty.HealthCheckUrl) == 0 {
		discoveryProperty.HealthCheckUrl =
			fmt.Sprintf("http://%v:%v/health",
				discoveryProperty.IpAddress,
				discoveryProperty.Port)
	}

	return &discoveryClient{
		consulProperty:    consulProperty,
		discoveryProperty: discoveryProperty,
	}
}

func (f *discoveryClient) OnAppStart() {
	if !f.discoveryProperty.Enabled {
		slago.Logger().Info().Msg("consul discovery is disabled, skipping")
		return
	}

	config := api.DefaultConfig()
	config.Address = f.consulProperty.Address
	config.TLSConfig.InsecureSkipVerify = f.consulProperty.TLSSkipVerify
	client, err := api.NewClient(config)
	if err != nil {
		slago.Logger().Fatal().Err(err).Msg("")
	}

	svc := &api.AgentServiceRegistration{
		ID:      f.discoveryProperty.ServiceId,
		Name:    f.discoveryProperty.ServiceName,
		Address: f.discoveryProperty.IpAddress,
		Port:    f.discoveryProperty.Port,
		Tags:    f.discoveryProperty.Tags,
		Check: &api.AgentServiceCheck{
			TLSSkipVerify: f.consulProperty.TLSSkipVerify,
			Method:        "GET",
			Timeout:       f.consulProperty.Timeout,
			Interval:      f.discoveryProperty.HealthCheckInterval,
			HTTP:          f.discoveryProperty.HealthCheckUrl,
		},
	}

	if err := client.Agent().ServiceRegister(svc); err != nil {
		slago.Logger().Fatal().Err(err).Msgf("fail to register service: %v",
			f.discoveryProperty.ServiceName)
	}

	f.consulClient = client

	slago.Logger().Info().Msgf("service %v has registerd into consul server",
		f.discoveryProperty.ServiceName)
}

func (f *discoveryClient) OnAppStop() {
	slago.Logger().Info().Msg("deregister this service from consul server")
	if err := f.consulClient.Agent().ServiceDeregister(
		f.discoveryProperty.ServiceId); err != nil {
		slago.Logger().Error().Err(err).Msg("consul deregister service failed")
	}
}

func getIp() string {
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		return ""
	}

	for _, address := range addrs {
		if ipnet, ok := address.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				return ipnet.IP.String()
			}
		}
	}

	return ""
}
